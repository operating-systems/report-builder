# Report Builder

## Usage 

You can use this Docker image to build markdown based reports for
the Operating Systems lecture.

The simplest usage is to add the following `.gitlab-ci.yml` to your repository :

```
image: $CI_REGISTRY/operating-systems/report-builder:latest

build-changed:
  only:
    - master
  except:
    - web
  script:
    - find reports -name "*.md" | xargs build-report

build-all:
  variables:
    TP: xx
  only:
    - web
  script:
    - test $TP = "xx" && echo "Please, set the TP variable" && false
    - find reports -name "report_${TP}.md" | xargs build-report
  artifacts:
    paths:
      - pdf/*.pdf
    expire_in: 15 day
```
