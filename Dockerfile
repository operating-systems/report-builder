FROM debian:buster
RUN apt-get update && apt-get install -y \
    git \
    pandoc \
    librsvg2-bin \
    pandoc-citeproc \
    texlive-latex-extra \
    texlive-latex-recommended \
    texlive-luatex \
    texlive-xetex \
    wkhtmltopdf

WORKDIR /tmp/
COPY OpenType.zip .

WORKDIR /usr/local/share/fonts
RUN unzip /tmp/OpenType.zip \
  && rm /tmp/OpenType.zip \
  && fc-cache -f -v

WORKDIR /usr/local/bin
COPY build-report.sh build-report
RUN chmod +x build-report

WORKDIR /root
