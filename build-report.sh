#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"

[ -d pdf ] || mkdir pdf
for i in $*; do
	NAME=$(basename $(dirname $i))
	SRC=$i
	DST=pdf/$(basename $i .md)_${NAME}.pdf
	echo "Building $SRC"
	pandoc ${SRC} -o ${DST} --resource-path=$(dirname $i) \
                  -V geometry:a4paper -V geometry:margin=2cm \
                  --pdf-engine=xelatex -V mainfont="IBM Plex Sans"
done
